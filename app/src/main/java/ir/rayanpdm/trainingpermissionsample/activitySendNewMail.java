package ir.rayanpdm.trainingpermissionsample;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class activitySendNewMail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_new_mail);

        Button b = findViewById(R.id.sendMail_Button);
        b.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText receiverEmailAddressEditText = findViewById(R.id.emailAddress_EditText);
                        EditText emailSubjectEditText = findViewById(R.id.emailSubject_EditText);
                        EditText emailContentEditText = findViewById(R.id.emailContent_EditText);

                        String receiverEmailAddress = String.valueOf(receiverEmailAddressEditText.getText());
                        String emailSubject = String.valueOf(emailSubjectEditText.getText());
                        String emailContent = String.valueOf(emailContentEditText.getText());
                        String []receivers = new String[1];
                        receivers[0] = receiverEmailAddress;
                        composeEmail(receivers,emailSubject,emailContent);
                    }
                }
        );
    }

    public void composeEmail(String[] addresses, String subject, String content) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, content);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
