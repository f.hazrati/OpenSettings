package ir.rayanpdm.trainingpermissionsample;

import android.content.Intent;
import android.provider.AlarmClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class activityCreateTimer extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_timer);

        Button addTimerBtn = findViewById(R.id.addTimerBtn);
        addTimerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText SecondsEditText = findViewById(R.id.secondsEditText);
                EditText MessagetEditText = findViewById(R.id.messagetEditText);

                String Seconds = String.valueOf(SecondsEditText.getText());
                int secondsInteger = -1;

                try{
                    secondsInteger = Integer.parseInt(Seconds);
                }
                catch(Exception ex){
                    SecondsEditText.setError("Seconds is not in correct format");
                    return;
                }

                String MessageFromEditText = String.valueOf(MessagetEditText.getText());

                startTimer(MessageFromEditText,secondsInteger);
            }
        });
    }
    public void startTimer(String message, int seconds) {
        Intent intent = new Intent(AlarmClock.ACTION_SET_TIMER)
                .putExtra(AlarmClock.EXTRA_MESSAGE, message)
                .putExtra(AlarmClock.EXTRA_LENGTH, seconds)
                .putExtra(AlarmClock.EXTRA_SKIP_UI, true);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
