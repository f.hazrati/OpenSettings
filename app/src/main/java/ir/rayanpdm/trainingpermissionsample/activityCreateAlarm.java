package ir.rayanpdm.trainingpermissionsample;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.provider.AlarmClock;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class activityCreateAlarm extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_alarm);
        Button addAlarmBtn = findViewById(R.id.addAlarmBtn);
        addAlarmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText HourEditText = findViewById(R.id.firstEditText);
                EditText MinuteEditText = findViewById(R.id.secondEditText);

                String Hour = String.valueOf(HourEditText.getText());
                int hourInteger = -1;

                try{
                    hourInteger = Integer.parseInt(Hour);
                    if(hourInteger > 23 || hourInteger < 0){
                        HourEditText.setError("Hour is not in correct format");
                        return;
                    }
                }
                catch(Exception ex){
                    HourEditText.setError("Hour is not in correct format");
                    return;
                }

                String Minute = String.valueOf(MinuteEditText.getText());
                int minuteInteger = -1;

                try{
                    minuteInteger = Integer.parseInt(Minute);
                    if(minuteInteger > 59 || minuteInteger < 0){
                        MinuteEditText.setError("Minute is not in correct format");
                        return;
                    }
                }
                catch(Exception ex){
                    MinuteEditText.setError("Minute is not in correct format");
                    return;
                }

                createAlarm("Android Fridays...",hourInteger ,minuteInteger);
            }
        });
    }
    public void createAlarm(String message, int hour, int minutes) {
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM)
                .putExtra(AlarmClock.EXTRA_MESSAGE, message)
                .putExtra(AlarmClock.EXTRA_HOUR, hour)
                .putExtra(AlarmClock.EXTRA_MINUTES, minutes);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }else{
            Toast.makeText(this,"getPackageManager() is null",Toast.LENGTH_LONG);
        }
    }
}
