package ir.rayanpdm.trainingpermissionsample;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Bitmap;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.bumptech.glide.Glide;


import java.io.File;
import java.util.Date;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button gpsButton = findViewById(R.id.OpenGPS_Button);
        Button bluetoothButton = findViewById(R.id.OpenBluetooth_Button);
        Button wirelessButton = findViewById(R.id.OpenWireless_Button);
        Button createAlarmButton = findViewById(R.id.Create_Button);
        Button createTimerButton = findViewById(R.id.CreateTimer_Button);
        Button takePictureButton = findViewById(R.id.TakePicture_Button);
        Button sendEmailButton = findViewById(R.id.SendEmail_Button);
        Button dialPhoneNumber = findViewById(R.id.DialPhoneNumber_Button);
        final Button showOnMap = findViewById(R.id.ShowOnMap_Button);

        gpsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gpsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(gpsIntent, 3000);
            }
        });

        bluetoothButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if (bluetoothAdapter == null) {
                    Toast.makeText(MainActivity.this, "Bluetooth is not supported", Toast.LENGTH_LONG).show();
                }
                else
                {
                    Intent gpsIntent = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
                    startActivityForResult(gpsIntent, 4000);
                }
            }
        });

        wirelessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gpsIntent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                startActivityForResult(gpsIntent, 5000);
            }
        });

        createAlarmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent secondIntent = new Intent(MainActivity.this,activityCreateAlarm.class);
                startActivityForResult(secondIntent,6000);
            }
        });

        createTimerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent secondIntent = new Intent(MainActivity.this,activityCreateTimer.class);
                startActivityForResult(secondIntent,7000);
            }
        });

        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    EasyImage.openCamera(MainActivity.this , 0);
                }
                catch (Exception ex){
                    Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG);
                }

            }
        });

        sendEmailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent secondIntent = new Intent(MainActivity.this,activitySendNewMail.class);
                startActivityForResult(secondIntent, 8000);
            }
        });
        dialPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent secondIntent = new Intent(MainActivity.this, activityDialPhoneNumber.class);
                startActivityForResult(secondIntent, 9000);
            }
        });

        showOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMap(Uri.parse("geo:0,0?q=35.7247015,51.4209026(Sematec)"));
            }
        });
    }

    public void showMap(Uri geoLocation) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(geoLocation);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                ImageView imgView = findViewById(R.id.CameraPicture_ImageView);
                Glide.with(MainActivity.this)
                        .load( imageFile).
                        into(imgView);
            }
        });

        switch (requestCode){
            case 3000:
                Toast.makeText(this, "from GPS settings", Toast.LENGTH_LONG).show();
                break;
            case 4000:
                Toast.makeText(this, "from Bluetooth settings", Toast.LENGTH_LONG).show();
                break;
            case 5000:
                Toast.makeText(this, "from Wireless settings", Toast.LENGTH_LONG).show();
                break;
            case 6000:
                Toast.makeText(this, "from Alarm settings", Toast.LENGTH_LONG).show();
                break;
            case 7000:
                Toast.makeText(this, "from Timer settings", Toast.LENGTH_LONG).show();
                break;
            case 8000:
                Toast.makeText(this, "from Email Sender", Toast.LENGTH_LONG).show();
                break;
            case 9000:
                Toast.makeText(this, "from Dial Phonenumber", Toast.LENGTH_LONG).show();
                break;
                default:
                    Toast.makeText(this, "Code is invalid", Toast.LENGTH_LONG).show();
        }
    }
}
